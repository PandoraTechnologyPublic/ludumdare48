using System;
using UnityEngine;

namespace LudumDare48
{
    public class CameraController : MonoBehaviour
    {
        public float _minDistance = 2;
        public float _maxDistance = 10;
        public float _zoomSpeed = 1;
        public float _panToZoomLevelRatio = 1;
        public float _sensitivity = 1;
        public Camera _camera;
        public Transform _target;
        public Transform _rotationNodeX;
        public Transform _rotationNodeY;

        private Vector3 _previousMousePosition;
        private void Update()
        {
            transform.position = _target.position;

            float scrollWheelAxis = Input.GetAxis("Mouse ScrollWheel");

            if (Mathf.Abs(scrollWheelAxis) > 0)
            {
                // _camera.transform.Translate(new Vector3(0, 0, 1 * Mathf.Sign(scrollWheelAxis)), Space.Self);
                Vector3 currentCameraLocalPosition = _camera.transform.localPosition;

                currentCameraLocalPosition.z = Mathf.Clamp(currentCameraLocalPosition.z + _zoomSpeed * Mathf.Sign(scrollWheelAxis), -_maxDistance, -_minDistance); // Min/Max Inverted because camera look towards +z

                _camera.transform.localPosition = currentCameraLocalPosition;
            }

            if (Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2))
            {
                _previousMousePosition = Input.mousePosition;
            }

            if (Input.GetMouseButton(1))
            {
                Vector3 delta = Input.mousePosition - _previousMousePosition;
                float cameraPanZoomLevelRatio = _camera.transform.localPosition.z * _panToZoomLevelRatio * _panToZoomLevelRatio;

                _rotationNodeY.Rotate(Vector3.up, delta.x * _sensitivity /* cameraPanZoomLevelRatio*/, Space.Self);
                _rotationNodeX.Rotate(Vector3.right, -delta.y * _sensitivity /* cameraPanZoomLevelRatio*/, Space.Self);

                _previousMousePosition = Input.mousePosition;
            }

            // if (Input.GetMouseButton(2))
            // {
            //     Vector3 delta = Input.mousePosition - _previousMousePosition;
            //     float cameraPanZoomLevelRatio = _camera.transform.localPosition.z * _panToZoomLevelRatio * _panToZoomLevelRatio;
            //
            //     _rotationNode.Rotate(_rotationNode.forward, delta.x /* cameraPanZoomLevelRatio*/, Space.World);
            //     _rotationNode.Rotate(_rotationNode.forward, - delta.y /* cameraPanZoomLevelRatio*/, Space.World);
            //
            //     _previousMousePosition = Input.mousePosition;
            // }
        }
    }
}
