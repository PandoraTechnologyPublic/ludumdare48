using System;
using LudumDare48.GameEvents;
using LudumDare48.GameState;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace LudumDare48
{
    public class MainMenuUiLayout : MonoBehaviour
    {
        [SerializeField] private Button _startGameButton;

        private EventManager _eventManager;

        [Inject]
        public void InitialisedDependencies(EventManager eventManager)
        {
            gameObject.SetActive(false);

            eventManager.Register<EnterGameStateEvent>(OnEnterGameState);
            eventManager.Register<ExitGameStateEvent>(OnExitGameState);

            _eventManager = eventManager;
        }

        private void Start()
        {
            _startGameButton.onClick.AddListener(OnStartGameButton);
        }

        private void OnStartGameButton()
        {
            _eventManager.TriggerEvent(new ChangeStateEvent(typeof(IngameState)));
        }

        private void OnEnterGameState(GameEvent gameEvent)
        {
            EnterGameStateEvent enterGameStateEvent = gameEvent as EnterGameStateEvent;

            if (enterGameStateEvent._stateType == typeof(MainMenuState))
            {
                gameObject.SetActive(true);
            }
        }

        private void OnExitGameState(GameEvent gameEvent)
        {
            ExitGameStateEvent exitGameStateEvent = gameEvent as ExitGameStateEvent;

            if (exitGameStateEvent._stateType == typeof(MainMenuState))
            {
                gameObject.SetActive(false);
            }
        }
    }
}