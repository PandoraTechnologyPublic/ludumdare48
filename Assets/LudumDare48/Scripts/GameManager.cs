using System;
using LudumDare48.GameState;
using UnityEngine;
using Zenject;

namespace LudumDare48
{
    public class GameManager : MonoBehaviour
    {
        public StateMachine<GameManager> _gameStateMachine;

        private DiContainer _diContainer;

        [Inject]
        public void InitialiseDependencies(DiContainer diContainer)
        {
            _diContainer = diContainer;
        }

        private void Start()
        {
            _gameStateMachine = new StateMachine<GameManager>();
            _diContainer.Inject(_gameStateMachine);
            _gameStateMachine.ChangeState<MainMenuState>(this);
        }

        private void Update()
        {
            _gameStateMachine.Update(this, Time.deltaTime);
        }

        // [Inject]
        // public public InitialiseDependencies()
        // {
        //
        // }
        public void ChangeState<TYPE_STATE>() where TYPE_STATE : State<GameManager>, new()
        {
            _gameStateMachine.ChangeState<TYPE_STATE>(this);
        }
    }
}
