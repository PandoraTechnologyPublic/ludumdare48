using LudumDare48.Machine;

namespace LudumDare48.GameEvents
{
    public class DisplayPhotoEvent : GameEvent
    {
        public readonly PhotoSceneComponent _photoSceneComponent;
        public readonly string _nextSceneName;

        public DisplayPhotoEvent(PhotoSceneComponent photoSceneComponent, string nextSceneName)
        {
            _photoSceneComponent = photoSceneComponent;
            _nextSceneName = nextSceneName;
        }

        public override string ToString()
        {
            return _photoSceneComponent.name;
        }
    }
}