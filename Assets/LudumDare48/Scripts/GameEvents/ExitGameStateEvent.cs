using System;

namespace LudumDare48.GameEvents
{
    public class ExitGameStateEvent : GameEvent
    {
        public readonly Type _stateType;

        public ExitGameStateEvent(Type stateType)
        {
            _stateType = stateType;
        }

        public override string ToString()
        {
            return _stateType.ToString();
        }
    }
}