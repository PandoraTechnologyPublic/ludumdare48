using System;

namespace LudumDare48.GameEvents
{
    public class EnterGameStateEvent : GameEvent
    {
        public readonly Type _stateType;

        public EnterGameStateEvent(Type stateType)
        {
            _stateType = stateType;
        }

        public override string ToString()
        {
            return _stateType.ToString();
        }
    }
}