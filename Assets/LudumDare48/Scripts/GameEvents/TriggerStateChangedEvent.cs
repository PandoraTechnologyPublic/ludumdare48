using LudumDare48.Machine;

namespace LudumDare48.GameEvents
{
    public class TriggerStateChangedEvent : GameEvent
    {
        public TriggerType TriggerType { get; }
        public int Value { get; }
        public readonly float _rangePosition;

        public TriggerStateChangedEvent(TriggerType triggerType,
            int value,
            float rangeMaxValue = -1
            )
        {
            TriggerType = triggerType;
            Value = value;
            _rangePosition = rangeMaxValue;
        }

        public override string ToString()
        {
            return $"{TriggerType} - {Value} - {_rangePosition:P}";
        }
    }
}