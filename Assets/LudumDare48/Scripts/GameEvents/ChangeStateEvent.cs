using System;

namespace LudumDare48.GameEvents
{
    public class ChangeStateEvent : GameEvent
    {
        public Type _newStateType;

        public ChangeStateEvent(Type newStateType)
        {
            _newStateType = newStateType;
        }

        public override string ToString()
        {
            return _newStateType.ToString();
        }
    }
}