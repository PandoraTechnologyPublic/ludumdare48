using NotImplementedException = System.NotImplementedException;

namespace LudumDare48.GameEvents
{
    public class FadeToNextSceneRequestEvent : GameEvent
    {
        public string _nextSceneName;

        public FadeToNextSceneRequestEvent(string nextSceneName)
        {
            _nextSceneName = nextSceneName;
        }
    }
}