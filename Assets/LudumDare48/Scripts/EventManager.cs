using System;
using System.Collections.Generic;
using LudumDare48.GameEvents;
using UnityEngine;

namespace LudumDare48
{
    public class EventManager
    {
        private readonly Dictionary<Type, List<Action<GameEvent>>> _eventListener = new Dictionary<Type, List<Action<GameEvent>>>();

        private int _triggerLevel = 0;
        private readonly List<Action<GameEvent>> _eventToUnregister = new List<Action<GameEvent>>();

        public void TriggerEvent<TYPE_EVENT>(TYPE_EVENT gameEvent) where TYPE_EVENT : GameEvent
        {
            Debug.Log($"EVENT TRIGGERED: <color=#ffa500ff>{typeof(TYPE_EVENT).Name}</color>: <size=10>{gameEvent}</size>");

            if (_eventListener.TryGetValue(typeof(TYPE_EVENT), out List<Action<GameEvent>> actions))
            {
                ++_triggerLevel;

                for (int actionIndex = 0; actionIndex < actions.Count; actionIndex++)
                {
                    Action<GameEvent> action = actions[actionIndex];
                    action(gameEvent);
                }

                --_triggerLevel;
            }

            if (_triggerLevel <= 0)
            {
                foreach (Action<GameEvent> eventToUnregisterAction in _eventToUnregister)
                {
                    if (_eventListener.TryGetValue(typeof(TYPE_EVENT), out List<Action<GameEvent>> actionList))
                    {
                        actionList.Remove(eventToUnregisterAction);
                    }
                }
            }
        }

        public void Register<TYPE_EVENT>(Action<GameEvent> eventAction) where TYPE_EVENT : GameEvent
        {
            if (!_eventListener.TryGetValue(typeof(TYPE_EVENT), out List<Action<GameEvent>> actions))
            {
                actions = new List<Action<GameEvent>>();
                _eventListener.Add(typeof(TYPE_EVENT), actions);
            }

            actions.Add(eventAction);
        }

        public void Unregister<TYPE_EVENT>(Action<GameEvent> eventAction) where TYPE_EVENT : GameEvent
        {
            _eventToUnregister.Add(eventAction);

            if (_triggerLevel <= 0)
            {
                foreach (Action<GameEvent> eventToUnregisterAction in _eventToUnregister)
                {
                    if (_eventListener.TryGetValue(typeof(TYPE_EVENT), out List<Action<GameEvent>> actionList))
                    {
                        actionList.Remove(eventToUnregisterAction);
                    }
                }
            }
        }
    }
}
