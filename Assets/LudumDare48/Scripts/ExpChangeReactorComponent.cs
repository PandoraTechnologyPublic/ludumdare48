using LudumDare48.GameEvents;
using LudumDare48.Machine;
using UnityEngine;
using Zenject;

namespace LudumDare48
{
    public class ExpChangeReactorComponent : MonoBehaviour
    {
        public TriggerType _triggerType;

        private EventManager _eventManager;

        [Inject]
        public void InitDependencies(EventManager eventManager)
        {
            _eventManager = eventManager;
        }

        private void Start()
        {
            _eventManager.Register<TriggerStateChangedEvent>(OnTriggerStateChanged);
        }

        private void OnDestroy()
        {
            _eventManager.Unregister<TriggerStateChangedEvent>(OnTriggerStateChanged);
        }

        private void OnTriggerStateChanged(GameEvent gameEvent)
        {
            TriggerStateChangedEvent triggerStateChangedEvent = (TriggerStateChangedEvent) gameEvent;

            if (triggerStateChangedEvent.TriggerType == _triggerType)
            {
                PhotoParameters photoParameters = GetComponent<PhotoParameters>();
                float photoParametersExp = (photoParameters.maxExp - photoParameters.minExp) * triggerStateChangedEvent._rangePosition;
                photoParameters.SetExpValue(photoParametersExp);
            }
        }
    }
}