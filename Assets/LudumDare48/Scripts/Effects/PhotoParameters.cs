using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DirectionalBlurPackage;

public class PhotoParameters : MonoBehaviour
{
    [SerializeField]
    [Range(0f, 1f)]
    public float
        DOF, FOV, Blur, Exp;
    public float
        minDof = 1, maxDof = 60,
        minFOV = 20, maxFOV = 90,
        minBlur = 0, maxBlur = 0.2f,
        minExp = 0.1f, maxExp = 10;

    private SyncEffects sync;
    private Camera cam;
    private AnisotropicDirectionalBlur blur;
    // Start is called before the first frame update
    void Start()
    {
        sync = gameObject.GetComponent<SyncEffects>();
        cam = gameObject.GetComponent<Camera>();
        blur = gameObject.GetComponent<AnisotropicDirectionalBlur>();
    }

    // Update is called once per frame
    void Update()
    {
        sync.dofDistance = map(DOF, 0, 1, minDof, maxDof);
        sync.exposure = map(Exp, 0, 1, minExp, maxExp);
        cam.fieldOfView = map(FOV, 0, 1, minFOV, maxFOV);
        blur.stretchDist = map(Blur, 0, 1, minBlur, maxBlur);
    }
    float map(float s, float a1, float a2, float b1, float b2)
    {
        return b1 + (s - a1) * (b2 - b1) / (a2 - a1);
    }

    public void SetFovValue(int fovValue)
    {
        FOV = fovValue / (maxFOV - minFOV);
    }

    public void SetDofValue(int dofValue)
    {
        DOF = dofValue / (maxDof - minDof);
    }

    public void SetBlurValue(float blurValue)
    {
        Blur = blurValue / (maxBlur - minBlur);
    }

    public void SetExpValue(float expValue)
    {
        Exp = expValue / (maxExp - minExp);
    }
}
