using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class SyncEffects : MonoBehaviour
{
    public float
        dofDistance = 10f,
        exposure = 1f;
    private PostProcessEffectSettings myPostProcessEffectSettings;
    private DepthOfField dof;
    private AutoExposure exp;
    // Start is called before the first frame update
    void Start()
    {
        // somewhere during initializing
        PostProcessVolume volume = gameObject.GetComponent<PostProcessVolume>();
        volume.profile.TryGetSettings(out dof);
        volume.profile.TryGetSettings(out exp);
    }

    // Update is called once per frame
    void Update()
    {
        dof.focusDistance.value = dofDistance;
        exp.keyValue.value = exposure;

        //gameObject.GetComponent<PostProcessVolume>().profile.TryGetSettings(out myPostProcessEffectSettings);
        //myPostProcessEffectSettings.yourParameter.value = newValue;
    }
}
