using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CopyFOV : MonoBehaviour
{
    public Camera CamToCopyFrom;
    private Camera cam;
    // Start is called before the first frame update
    void Start()
    {
        cam = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        cam.fieldOfView = CamToCopyFrom.fieldOfView;
    }
}
