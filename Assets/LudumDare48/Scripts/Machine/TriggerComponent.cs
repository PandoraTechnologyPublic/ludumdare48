using LudumDare48.GameEvents;
using LudumDare48.System;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using Zenject;

namespace LudumDare48.Machine
{
    public class TriggerComponent : MonoBehaviour
    {
        [SerializeField] protected TriggerType _triggerType;
        [SerializeField] private Sprite _cursorSprite;
        [SerializeField] protected int _currentValue = 0;

        protected EventManager _eventManager;
        private CursorDecoratorLayout _cursorDecoratorLayout;

        public Transform _transform;

        [Inject]
        public void InitDependencies(EventManager eventManager, CursorDecoratorLayout cursorDecoratorLayout)
        {
            _eventManager = eventManager;
            _cursorDecoratorLayout = cursorDecoratorLayout;
        }

        protected void Awake()
        {
            if (_transform == null)
            {
                _transform = transform;
            }

            _eventManager.Register<TriggerStateChangedEvent>(OnTriggerStateChanged);
        }

        private void OnDestroy()
        {
            _eventManager.Unregister<TriggerStateChangedEvent>(OnTriggerStateChanged);
        }

        private void OnEnable()
        {
            TriggerEvent();
        }

        private void OnMouseEnter()
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                _cursorDecoratorLayout.SetCursorSprite(_cursorSprite);
            }
        }

        private void OnMouseExit()
        {
            _cursorDecoratorLayout.UnsetCursorSprite();
        }

        protected void TriggerEvent()
        {
            _eventManager.TriggerEvent(new TriggerStateChangedEvent(_triggerType, _currentValue));
        }

        protected void TriggerEvent(int currentValue, int rangeMinValue, int rangeMaxValue)
        {
            Assert.IsTrue(rangeMinValue < rangeMaxValue);

            currentValue = Mathf.RoundToInt(Mathf.Clamp(currentValue, rangeMinValue, rangeMaxValue));
            float valueInRange = (currentValue - rangeMinValue) / (float) (rangeMaxValue - rangeMinValue);//Mathf.Clamp((currentValue - rangeMinValue) / (float) (rangeMaxValue - rangeMinValue), 0, 1);

            _eventManager.TriggerEvent(new TriggerStateChangedEvent(_triggerType, currentValue, valueInRange));
        }

        private void OnTriggerStateChanged(GameEvent gameEvent)
        {
            OnTriggerStateChanged((TriggerStateChangedEvent) gameEvent);
        }

        protected virtual void OnTriggerStateChanged(TriggerStateChangedEvent gameEvent)
        {
        }
    }
}