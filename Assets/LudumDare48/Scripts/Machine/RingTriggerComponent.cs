using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace LudumDare48.Machine
{
    public class RingTriggerComponent : TriggerComponent
    {
        public Vector3 _axisRotation = Vector3.forward;
        public int _minValue = 0;
        public int _initialValue = 5;
        public int _maxValue = 10;
        public int _minAngleValue = 10;
        public int _maxAngleValue = 100;
        public float _snapSpeed = 20f;
        public bool _shouldSnapWhenDragging = true;

        private CameraController _cameraController;
        private AudioSource _audioSource;
        private Vector3 _initialHitLocal;
        private Vector3 _planeHitLocal;
        private Matrix4x4 _baseMatrix;
        private Plane _plane;
        private Collider _collider;
        private float _dragAngle;
        private float _baseAngle;
        private float _newAngle;
        private float _snapAngle;
        private int _previousValue;
        private bool _snapping;

        protected void Start()
        {
            _currentValue = _initialValue;

            UpdateAngleValue(_minAngleValue, _maxAngleValue);
            _cameraController = GameObject.FindObjectOfType<CameraController>();
            _audioSource = GetComponent<AudioSource>();


            _collider = GetComponentInChildren<Collider>();
            _newAngle = _snapAngle = GetValueAngelContribution() * _currentValue + _minAngleValue;
            _previousValue = _currentValue;
        }

        private void Update()
        {
            if(_snapping)
            {
                float valueAngelContribution = GetValueAngelContribution();
                float targetAngle = _currentValue * valueAngelContribution + _minAngleValue;
                _snapAngle = Mathf.Lerp(_snapAngle, targetAngle, Time.deltaTime * _snapSpeed);
                _transform.localRotation = Quaternion.AngleAxis(_snapAngle, _axisRotation);
                if (Mathf.Approximately(_snapAngle, targetAngle))
                {
                    _snapping = false;
                }
            }
        }

        private void OnMouseDown()
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                Ray ray = _cameraController._camera.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (_collider.Raycast(ray, out hit, 30.0f))
                {
                    _plane = new Plane(transform.localToWorldMatrix.MultiplyVector(_axisRotation), hit.point);
                    _initialHitLocal = transform.worldToLocalMatrix.MultiplyPoint(hit.point);
                    _initialHitLocal.Scale(Vector3.one - _axisRotation);
                    _baseAngle = _newAngle;
                    if (_snapping)
                    {
                        _newAngle = _baseAngle = _snapAngle;
                    }
                    _baseMatrix = transform.worldToLocalMatrix;
                }
            }
        }

        private void UpdateCurrentValue()
        {
            float valueAngelContribution = GetValueAngelContribution();
            _currentValue = Mathf.RoundToInt((_newAngle - _minAngleValue) / valueAngelContribution);
            if (_currentValue != _previousValue)
            {
                PlaySound();
                TriggerEvent(_currentValue, _minValue, _maxValue);
                _previousValue = _currentValue;
            }
        }

        private void PlaySound()
        {
            if (_audioSource != null)
            {
                _audioSource.loop = false;
                _audioSource.Play();
            }
        }

        private void OnMouseDrag()
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                float enter;
                Ray ray = _cameraController._camera.ScreenPointToRay(Input.mousePosition);
                if (_plane.Raycast(ray, out enter))
                {
                    _planeHitLocal = _baseMatrix.MultiplyPoint(ray.GetPoint(enter));
                    _planeHitLocal.Scale(Vector3.one - _axisRotation);
                    _dragAngle = Vector3.SignedAngle(_initialHitLocal, _planeHitLocal, _axisRotation);
                    _newAngle = Mathf.Clamp(_baseAngle + _dragAngle, _minAngleValue, _maxAngleValue);
                    if (_shouldSnapWhenDragging)
                    {
                        _snapping = true;
                    }
                    else
                    {
                        _transform.localRotation = Quaternion.AngleAxis(_newAngle, _axisRotation);
                        _snapAngle = _newAngle;
                    }
                    UpdateCurrentValue();
                }
            }
        }

        private void OnMouseUp()
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                float valueAngelContribution = GetValueAngelContribution();
                UpdateCurrentValue();
                float currentAngle = _currentValue * valueAngelContribution;
                _snapping = true;

                TriggerEvent(_currentValue, _minValue, _maxValue);
            }
        }

        private float GetValueAngelContribution()
        {
            int angleRange = _maxAngleValue - _minAngleValue;
            int valueRange = _maxValue - _minValue;
            float valueAngelContribution = angleRange / (float) valueRange;

            return valueAngelContribution;
        }

        public void UpdateAngleValue(int minAngle, int maxAngle)
        {
            _minAngleValue = minAngle;
            _maxAngleValue = maxAngle;

            float valueAngelContribution = GetValueAngelContribution();
            float currentAngle = _currentValue * valueAngelContribution;

            _transform.localRotation = Quaternion.AngleAxis(_minAngleValue + currentAngle, _axisRotation); ;

            TriggerEvent(_currentValue, _minValue, _maxValue);
        }
    }
}