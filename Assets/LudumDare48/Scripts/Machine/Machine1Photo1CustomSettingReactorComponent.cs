using LudumDare48.GameEvents;
using NotImplementedException = System.NotImplementedException;

namespace LudumDare48.Machine
{
    public class Machine1Photo1CustomSettingReactorComponent : ReactorComponent
    {
        private int _currentSettingIndex = 0;

        protected override void ActivatorProcessing(int value)
        {
            PhotoParameters photoParameters = GetComponent<PhotoParameters>();

            if (value == 1)
            {
                _currentSettingIndex = (_currentSettingIndex + 1) % 4;

                switch (_currentSettingIndex)
                {
                    case 0:
                    {
                        photoParameters.Blur = 0f;
                        photoParameters.Exp = 0.010f;
                        photoParameters.DOF = 0.166f;
                        photoParameters.FOV = 0.69f;
                        break;
                    }
                    case 1:
                    {
                        photoParameters.Blur = 0;
                        photoParameters.Exp = 0.1f;
                        photoParameters.DOF = 0.4f;
                        photoParameters.FOV = 0.8f;
                        break;
                    }
                    case 2:
                    {
                        photoParameters.Blur = 0.2f;
                        photoParameters.Exp = 0.1f;
                        photoParameters.DOF = 0.35f;
                        photoParameters.FOV = 0.9f;
                        break;
                    }
                    case 3:
                    {
                        photoParameters.Blur = 0f;
                        photoParameters.Exp = 0.3f;
                        photoParameters.DOF = 0.7f;
                        photoParameters.FOV = 1;
                        break;
                    }
                }

                _eventManager.TriggerEvent(new TriggerStateChangedEvent(TriggerType.SUMAFO_MACHINE_SCREEN_PHOTO_INDEX_STATE, _currentSettingIndex));
            }
        }
    }
}