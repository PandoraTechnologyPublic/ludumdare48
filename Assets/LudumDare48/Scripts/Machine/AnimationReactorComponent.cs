using UnityEngine;

namespace LudumDare48.Machine
{
    public class AnimationReactorComponent : ReactorComponent
    {
        public Animator _controlledAnimation;

        protected override void ActivatorProcessing(int value)
        {
            if (value == 10)
            {
                _controlledAnimation.SetTrigger("Open");
            }
            else if (value == 0)
            {
                _controlledAnimation.SetTrigger("Close");
            }
        }
    }
}