using LudumDare48.GameEvents;
using UnityEngine;
using Zenject;
using NotImplementedException = System.NotImplementedException;

namespace LudumDare48.Machine
{
    public class Machine2Photo1CustomSettingReactorComponent : ReactorComponent
    {
        protected override void ActivatorProcessing(int value)
        {
            PhotoParameters photoParameters = GetComponent<PhotoParameters>();

            switch (value)
            {
                case 0:
                {
                    photoParameters.Blur = 0.8f;
                    photoParameters.Exp = 0.010f;
                    photoParameters.DOF = 0.35f;
                    break;
                }
                case 1:
                {
                    photoParameters.Blur = 0;
                    photoParameters.Exp = 0.11f;
                    photoParameters.DOF = 0.4f;
                    break;
                }
                case 2:
                {
                    photoParameters.Blur = 0.3f;
                    photoParameters.Exp = 0.1f;
                    photoParameters.DOF = 0.35f;
                    break;
                }
                case 3:
                {
                    photoParameters.Blur = 1.0f;
                    photoParameters.Exp = 0.8f;
                    photoParameters.DOF = 0.7f;
                    break;
                }
            }
        }
    }
}