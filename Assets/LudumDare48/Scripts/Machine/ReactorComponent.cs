using LudumDare48.GameEvents;
using UnityEngine;
using Zenject;

namespace LudumDare48.Machine
{
    public abstract class ReactorComponent : MonoBehaviour
    {
        public TriggerType _triggerType;

        protected EventManager _eventManager;

        [Inject]
        public void InitDependencies(EventManager eventManager)
        {
            _eventManager = eventManager;
        }

        private void Start()
        {
            _eventManager.Register<TriggerStateChangedEvent>(OnTriggerStateChanged);
        }

        private void OnDestroy()
        {
            _eventManager.Unregister<TriggerStateChangedEvent>(OnTriggerStateChanged);
        }

        private void OnTriggerStateChanged(GameEvent gameEvent)
        {
            TriggerStateChangedEvent triggerStateChangedEvent = (TriggerStateChangedEvent) gameEvent;

            if (triggerStateChangedEvent.TriggerType == _triggerType)
            {
                ActivatorProcessing(triggerStateChangedEvent.Value);
            }
        }

        protected abstract void ActivatorProcessing(int value);
    }
}