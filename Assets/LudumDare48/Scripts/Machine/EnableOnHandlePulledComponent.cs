using LudumDare48.GameEvents;
using UnityEngine;
using Zenject;

namespace LudumDare48.Machine
{
    public class EnableOnHandlePulledComponent : MonoBehaviour
    {
        private EventManager _eventManager;

        private GameObject _visorToHide;

        [Inject]
        public void InitDependencies(EventManager eventManager)
        {
            _eventManager = eventManager;
        }

        public void Awake()
        {
            _eventManager.Register<TriggerStateChangedEvent>(OnTriggerStateChanged);

            VisorToHide.gameObject.SetActive(false);
        }

        public void OnDestroy()
        {
            _eventManager.Unregister<TriggerStateChangedEvent>(OnTriggerStateChanged);
        }

        private void OnTriggerStateChanged(GameEvent obj)
        {
            TriggerStateChangedEvent gameEvent = (TriggerStateChangedEvent) obj;

            if (gameEvent.TriggerType == TriggerType.STEAM_MACHINE_HANDLE)
            {
                if (VisorToHide != null)
                {
                    bool shouldShowVisor = gameEvent._rangePosition > 0.9f;
                    VisorToHide.SetActive(shouldShowVisor);
                }
            }
        }

        private GameObject VisorToHide
        {
            get
            {
                if (_visorToHide == null)
                {
                    PhotoMachineValidatorComponent photoMachineValidatorComponent = GetComponent<PhotoMachineValidatorComponent>();

                    if (photoMachineValidatorComponent != null)
                    {
                        _visorToHide = photoMachineValidatorComponent._visorToHide.gameObject;
                    }
                }

                return _visorToHide;
            }
        }
    }
}