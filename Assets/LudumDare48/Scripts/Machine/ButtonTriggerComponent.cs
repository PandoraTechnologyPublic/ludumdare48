using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

namespace LudumDare48.Machine
{
    public class ButtonTriggerComponent : TriggerComponent
    {
        [SerializeField] private Vector3 _pressedLocalMove;

        public AudioSource _audioSource;

        public Sequence _buttonSequence;
        public Vector3 _initialPosition;

        protected void Awake()
        {
            base.Awake();

            _audioSource = GetComponent<AudioSource>();
        }

        protected void OnMouseDown()
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                PressButton();
            }
        }

        protected void PressButton()
        {
            if (_buttonSequence == null)
            {
                _initialPosition = _transform.localPosition;

                _buttonSequence = DOTween.Sequence()
                    .Append(_transform.DOLocalMove(_initialPosition + _pressedLocalMove, 0.1f))
                    .AppendCallback(() =>
                    {
                        _currentValue = 1;
                        TriggerEvent();

                        if (_audioSource != null)
                        {
                            _audioSource.Play();
                        }
                    })
                    .AppendInterval(0.1f)
                    .Append(_transform.DOLocalMove(_initialPosition, 0.2f))
                    .OnComplete(() =>
                    {
                        _currentValue = 0;
                        TriggerEvent();
                        _buttonSequence = null;
                    });
            }
        }

        protected void OnMouseUp()
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                TriggerEvent();
            }
        }
    }
}