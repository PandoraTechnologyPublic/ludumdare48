using LudumDare48.GameEvents;
using UnityEngine;
using UnityEngine.EventSystems;

namespace LudumDare48.Machine
{
    public class LatchComponent : TriggerComponent
    {
        public Vector3 _axisRotation = Vector3.forward;
        public int _maxValue = 80;

        public int _notValidatedMaxAngle = 30;
        public int _maxAngle = 80;

        public float _sensibility = .05f;

        private Vector3 _mouseDownPosition;

        private bool _isHandleOpened;
        private AudioSource _audioSource;

        protected void Start()
        {
            _currentValue = 0;
            _audioSource = GetComponent<AudioSource>();
        }

        private void OnMouseDown()
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                _mouseDownPosition = Input.mousePosition;
            }
        }

        private void OnMouseDrag()
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                float maxAngleValue = (_isHandleOpened) ? _maxAngle : _notValidatedMaxAngle;
                float distance = Input.mousePosition.x - _mouseDownPosition.x;
                float rotationValue = Mathf.Clamp(_currentValue + distance * _sensibility, 0, maxAngleValue);

                if (_currentValue != Mathf.RoundToInt(rotationValue))
                {
                    _currentValue = Mathf.RoundToInt(rotationValue);

                    if (_audioSource != null)
                    {
                        _audioSource.loop = false;
                        _audioSource.Play();
                    }

                    _mouseDownPosition = Input.mousePosition;
                    TriggerEvent(_currentValue, 0, _maxValue);
                }

                _transform.localEulerAngles = _axisRotation * rotationValue;
            }
        }

        private void OnMouseUp()
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                float maxAngleValue = (_isHandleOpened) ? _maxAngle : _notValidatedMaxAngle;
                float distance = Input.mousePosition.x - _mouseDownPosition.x;
                float rotationValue = Mathf.Clamp(_currentValue + distance * _sensibility, 0, maxAngleValue);

                _currentValue = Mathf.RoundToInt(rotationValue);

                _transform.localEulerAngles = _axisRotation * rotationValue;

                TriggerEvent(_currentValue, 0, _maxValue);
            }
        }

        protected override void OnTriggerStateChanged(TriggerStateChangedEvent gameEvent)
        {
            if (gameEvent.TriggerType == TriggerType.STEAM_MACHINE_HANDLE)
            {
                _isHandleOpened = (gameEvent.Value >= 9);
            }
        }
    }
}