using DG.Tweening;
using LudumDare48.GameEvents;
using UnityEngine;

namespace LudumDare48.Machine
{
    public class Machine3ButtonTriggerComponent : ButtonTriggerComponent
    {
        public AudioClip _blockedAudioClip;
        public Vector3 _blockedLocalMoved;

        public bool _isHandleOpened;
        public bool _isLatchPut;

        public AudioClip _initialAudioClip;

        protected new void OnMouseDown()
        {
            if (_isHandleOpened && _isLatchPut)
            {
                PressButton();
            }
            else
            {
                PressButtonButBlocked();
            }
        }

        private void PressButtonButBlocked()
        {
            if (_buttonSequence == null)
            {
                _audioSource.PlayOneShot(_blockedAudioClip);

                _initialPosition = _transform.localPosition;

                _buttonSequence = DOTween.Sequence()
                    .Append(_transform.DOLocalMove(_initialPosition + _blockedLocalMoved, 0.1f))
                    .AppendInterval(0.05f)
                    .Append(_transform.DOLocalMove(_initialPosition, 0.1f))
                    .OnComplete(() =>
                    {
                        _buttonSequence = null;
                    });
            }
        }

        protected override void OnTriggerStateChanged(TriggerStateChangedEvent gameEvent)
        {
            base.OnTriggerStateChanged(gameEvent);

            if (gameEvent.TriggerType == TriggerType.STEAM_MACHINE_HANDLE)
            {
                _isHandleOpened = gameEvent._rangePosition >= .9f;
            }
            else if (gameEvent.TriggerType == TriggerType.STEAM_MACHINE_LOCK_LATCH)
            {
                _isLatchPut = gameEvent._rangePosition >= .6f;
            }
        }
    }
}