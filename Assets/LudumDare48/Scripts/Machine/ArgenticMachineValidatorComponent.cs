using System.Collections.Generic;
using UnityEngine.SceneManagement;

namespace LudumDare48.Machine
{
    public class ArgenticMachineValidatorComponent : MachineValidationComponent
    {
        private readonly Dictionary<string, int> _machineObjectives = new Dictionary<string, int>()
        {
            {TriggerType.ARGENTIC_MACHINE_SHUTTER_BUTTON.ToString(), 1}
        };

        private readonly Dictionary<TriggerType, int> _machineDefaultState = new Dictionary<TriggerType, int>()
        {
            {TriggerType.ARGENTIC_MACHINE_SHUTTER_BUTTON, 0}
        };

        protected override Dictionary<string, int> GetMachineObjectives()
        {
            return _machineObjectives;
        }

        protected override Dictionary<TriggerType, int> GetMachineDefaultState()
        {
            return _machineDefaultState;
        }

        protected override void OnValidatedStateChanged(bool isStateVerified)
        {
            if (isStateVerified)
            {
                StartCoroutine(LoadNextSceneCoroutine());
            }
        }
    }
}