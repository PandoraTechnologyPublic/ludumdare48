using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

namespace LudumDare48.Machine
{
    public class OnOffButtonTriggerComponent : TriggerComponent
    {
        [SerializeField] private Vector3 _pressedLocalMove;

        private Sequence _buttonSequence;
        private Vector3 _initialPosition;

        private void Start()
        {
            _initialPosition = _transform.localPosition;
        }

        private void OnMouseDown()
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                if (_buttonSequence == null)
                {
                    if (_currentValue == 0)
                    {
                        _buttonSequence = DOTween.Sequence()
                            .Append(transform.DOLocalMove(_initialPosition + _pressedLocalMove, 0.4f))
                            .AppendCallback(() =>
                            {
                                _currentValue = 1;
                                _buttonSequence = null;
                                TriggerEvent();
                            });
                    }

                    if (_currentValue == 1)
                    {
                        _buttonSequence = DOTween.Sequence()
                            .Append(transform.DOLocalMove(_initialPosition, 0.4f))
                            .AppendCallback(() =>
                            {
                                _currentValue = 0;
                                _buttonSequence = null;
                                TriggerEvent();
                            });
                    }

                    // .AppendInterval(0.1f)
                    // .Append(transform.DOLocalMove(_initialPosition, 0.2f))
                    // .OnComplete(() =>
                    // {
                    //     _currentValue = 0;
                    //     TriggerEvent();
                    //     _buttonSequence = null;
                    // });
                }
            }
        }

        private void OnMouseUp()
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                TriggerEvent();
            }
        }
    }
}