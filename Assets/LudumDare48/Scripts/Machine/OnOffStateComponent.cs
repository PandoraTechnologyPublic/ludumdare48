using System;
using LudumDare48.GameEvents;
using UnityEngine;

namespace LudumDare48.Machine
{
    public class OnOffStateComponent : TriggerComponent
    {
        private void OnEnable()
        {
            _currentValue = 1;
            TriggerEvent();
        }

        private void OnDisable()
        {
            _currentValue = 0;
            TriggerEvent();
        }
    }
}