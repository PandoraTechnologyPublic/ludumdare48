using System.Collections;
using System.Collections.Generic;
using LudumDare48.GameEvents;
using LudumDare48.GameState;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace LudumDare48.Machine
{
    public class MachineValidationComponent : MonoBehaviour
    {
        [SerializeField] protected string _nextSceneName;

        protected EventManager _eventManager;

        private Dictionary<TriggerType, int> _machineCurrentState;
        private bool _wasStateValidated = false;

        [Inject]
        public void InitDependencies(EventManager eventManager)
        {
            _eventManager = eventManager;
        }

        protected void Start()
        {
            _machineCurrentState = GetMachineDefaultState();

            _eventManager.Register<TriggerStateChangedEvent>(OnTriggerStateChanged);
        }

        private void OnDestroy()
        {
            _eventManager.Unregister<TriggerStateChangedEvent>(OnTriggerStateChanged);
        }

        protected void OnTriggerStateChanged(GameEvent gameEvent)
        {
            TriggerStateChangedEvent triggerStateChangedEvent = (TriggerStateChangedEvent) gameEvent;

            _machineCurrentState[triggerStateChangedEvent.TriggerType] = triggerStateChangedEvent.Value;

            ValidateState();
        }

        private void ValidateState()
        {
            bool isStateValidated = true;

            foreach (KeyValuePair<TriggerType, int> keyValuePair in GetMachineDefaultState())
            {
                TriggerType triggerType = keyValuePair.Key;
                int triggerValue = keyValuePair.Value;

                Dictionary<string, int> machineObjectives = GetMachineObjectives();

                if (machineObjectives.TryGetValue(triggerType.ToString(), out int objectiveValue)
                        && triggerValue != objectiveValue)
                {
                    isStateValidated = false;
                    break;
                }

                if (machineObjectives.TryGetValue($"{triggerType}_MIN", out int minValue)
                        && machineObjectives.TryGetValue($"{triggerType}_MAX", out int maxValue))
                {
                    if (triggerValue < minValue || triggerValue > maxValue)
                    {
                        isStateValidated = false;
                        break;
                    }
                }
            }

            if (_wasStateValidated != isStateValidated)
            {
                _wasStateValidated = isStateValidated;
                OnValidatedStateChanged(isStateValidated);
            }

            Debug.Log($"State validated: {isStateValidated}");
        }

        public bool IsValid()
        {
            return _wasStateValidated;
        }

        protected virtual Dictionary<string, int> GetMachineObjectives()
        {
            return new Dictionary<string, int>();
        }

        protected virtual Dictionary<TriggerType, int> GetMachineDefaultState()
        {
            return new Dictionary<TriggerType, int>();
        }

        protected virtual void OnValidatedStateChanged(bool isStateVerified)
        {
        }

        protected IEnumerator LoadNextSceneCoroutine()
        {
            if (_nextSceneName != null)
            {
                yield return new WaitForSeconds(1.0f);

                SceneManager.LoadSceneAsync(_nextSceneName);
            }
        }
    }
}