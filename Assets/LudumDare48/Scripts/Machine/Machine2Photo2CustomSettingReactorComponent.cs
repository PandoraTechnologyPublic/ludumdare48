using LudumDare48.GameEvents;
using UnityEngine;
using Zenject;
using NotImplementedException = System.NotImplementedException;

namespace LudumDare48.Machine
{
    public class Machine2Photo2CustomSettingReactorComponent : ReactorComponent
    {
        protected override void ActivatorProcessing(int value)
        {
            PhotoParameters photoParameters = GetComponent<PhotoParameters>();

            switch (value)
            {
                case 0:
                {
                    photoParameters.Blur = 0.2f;
                    photoParameters.Exp = 0.3f;
                    photoParameters.DOF = 0.85f;
                    break;
                }
                case 1:
                {
                    photoParameters.Blur = 0;
                    photoParameters.Exp = 0.4f;
                    photoParameters.DOF = 0.65f;
                    break;
                }
                case 2:
                {
                    photoParameters.Blur = 0.2f;
                    photoParameters.Exp = 0.4f;
                    photoParameters.DOF = 0.35f;
                    break;
                }
                case 3:
                {
                    photoParameters.Blur = 0.0f;
                    photoParameters.Exp = 0.289f;
                    photoParameters.DOF = 0.144f;
                    break;
                }
            }
        }
    }
}