using LudumDare48.GameEvents;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace LudumDare48.Machine
{
    public class TranslationChangeReactorComponent: MonoBehaviour
    {
        public Vector3 _translationVector;
        public TriggerType _triggerType;
        public float _minPositionValue = 10;
        public float _maxPositionValue = 100;

        private EventManager _eventManager;

        [Inject]
        public void InitDependencies(EventManager eventManager)
        {
            _eventManager = eventManager;
        }

        private void Start()
        {
            _eventManager.Register<TriggerStateChangedEvent>(OnTriggerStateChanged);
        }

        private void OnDestroy()
        {
            _eventManager.Unregister<TriggerStateChangedEvent>(OnTriggerStateChanged);
        }

        private void OnTriggerStateChanged(GameEvent gameEvent)
        {
            TriggerStateChangedEvent triggerStateChangedEvent = (TriggerStateChangedEvent) gameEvent;

            if (triggerStateChangedEvent.TriggerType == _triggerType)
            {
                // transform.localPosition = _translationVector * triggerStateChangedEvent.Value;
                // float clampedValue = Mathf.Clamp(transform.localPosition, _minPositionValue, _maxPositionValue);

                Assert.IsTrue(triggerStateChangedEvent._rangePosition >= 0);
                Assert.IsTrue(triggerStateChangedEvent._rangePosition <= 1);
                Assert.IsTrue(_minPositionValue < _maxPositionValue);

                float newVisualValue = (_maxPositionValue - _minPositionValue) * triggerStateChangedEvent._rangePosition;
                transform.localPosition = _translationVector * (_maxPositionValue - newVisualValue);
            }
        }
    }
}