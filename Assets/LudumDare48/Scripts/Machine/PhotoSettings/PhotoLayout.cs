using System;
using System.Collections.Generic;
using DG.Tweening;
using LudumDare48.GameEvents;
using LudumDare48.GameState;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;

namespace LudumDare48
{
    public class PhotoLayout : MonoBehaviour
    {
        public Button _target;

        public List<Button> _sequentialButtons;

        private EventManager _eventManager;
        private DisplayPhotoEvent _displayPhotoEvent;
        private IngameUiLayout _ingameUiLayout;
        private MainUiLayout _mainUiLayout;

        private int _pendingStoryCount = 0;

        [Inject]
        public void InitialiseDependencies(EventManager eventManager, MainUiLayout mainUiLayout)
        {
            _eventManager = eventManager;
            _mainUiLayout = mainUiLayout;
        }

        private void Start()
        {
            _target.onClick.AddListener(OnTargetClick);

            _target.gameObject.SetActive(false);

            foreach (Button sequentialButton in _sequentialButtons)
            {
                ++_pendingStoryCount;
                sequentialButton.onClick.AddListener(() => OnStoryButton(sequentialButton));
            }

            if (_pendingStoryCount <= 0)
            {
                _target.gameObject.SetActive(true);
            }
        }

        private void OnStoryButton(Button sequentialButton)
        {
            --_pendingStoryCount;
            sequentialButton.gameObject.SetActive(false);
            if (_pendingStoryCount <= 0)
            {
                _target.gameObject.SetActive(true);
            }
        }

        private void OnTargetClick()
        {
            _ingameUiLayout.HidePhoto();

            //_eventManager.TriggerEvent(); NEXT_PHOTO or NEXT_LEVEL
            if (!string.IsNullOrEmpty(_displayPhotoEvent._nextSceneName))
            {
                // yield return new WaitForSeconds(1.0f);

                if (_displayPhotoEvent._nextSceneName == "GameOverScene")
                {
                    void OnSceneManagerOnactiveSceneChanged(Scene arg0, Scene scene)
                    {
                        _eventManager.TriggerEvent(new ChangeStateEvent(typeof(GameOverState)));
                        GameObject.FindObjectOfType<PhotoAlbumLayout>()._albumPanel.gameObject.SetActive(true);
                        SceneManager.activeSceneChanged -= OnSceneManagerOnactiveSceneChanged;
                    }

                    SceneManager.activeSceneChanged += OnSceneManagerOnactiveSceneChanged;
                }

                _eventManager.TriggerEvent(new FadeToNextSceneRequestEvent(_displayPhotoEvent._nextSceneName));
            }
            else
            {
                // DOTween.Sequence().AppendInterval(2.0f).AppendCallback(() =>
                // {
                    _eventManager.TriggerEvent(new ChangeStateEvent(typeof(GameOverState)));
                // });
            }

        }

        public void SetDisplayEvent(DisplayPhotoEvent displayPhotoEvent, IngameUiLayout ingameUiLayout)
        {
            _displayPhotoEvent = displayPhotoEvent;
            _ingameUiLayout = ingameUiLayout;
        }
    }
}