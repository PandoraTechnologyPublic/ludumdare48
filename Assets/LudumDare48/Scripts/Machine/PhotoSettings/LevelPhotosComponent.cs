using System.Collections.Generic;
using UnityEngine;

namespace LudumDare48
{
    public class LevelPhotosComponent : MonoBehaviour
    {
        public List<PhotoSceneComponent> _photoScenes;
    }
}