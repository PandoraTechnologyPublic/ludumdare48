using System.Collections.Generic;

namespace LudumDare48.Machine.PhotoSettings
{
    public class Machine1Photo02ValidationComponent : PhotoMachineValidatorComponent
    {
        private readonly Dictionary<string, int> _machineObjectives = new Dictionary<string, int>()
        {
            { TriggerType.SUMAFO_MACHINE_SHUTTER_BUTTON.ToString(), 1 },
            { TriggerType.SUMAFO_MACHINE_SCREEN_PHOTO_INDEX_STATE.ToString(), 3 },
            { TriggerType.SUMAFO_MACHINE_SCREEN_STATE.ToString(), 1 },
        };

        private readonly Dictionary<TriggerType, int> _machineDefaultState = new Dictionary<TriggerType, int>()
        {
            { TriggerType.SUMAFO_MACHINE_SHUTTER_BUTTON, 0 },
            { TriggerType.SUMAFO_MACHINE_SCREEN_PHOTO_INDEX_STATE, 0 },
            { TriggerType.SUMAFO_MACHINE_SCREEN_STATE, 0 },
        };

        protected override Dictionary<string, int> GetMachineObjectives()
        {
            return _machineObjectives;
        }

        protected override Dictionary<TriggerType, int> GetMachineDefaultState()
        {
            return _machineDefaultState;
        }
    }
}