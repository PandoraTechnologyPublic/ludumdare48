using LudumDare48.GameEvents;
using UnityEngine;

namespace LudumDare48.Machine
{
    public class PhotoMachineValidatorComponent : MachineValidationComponent
    {
        public Transform _visorToHide;

        protected override void OnValidatedStateChanged(bool isStateVerified)
        {
            if (isStateVerified)
            {
                _eventManager.TriggerEvent(new DisplayPhotoEvent(GetComponent<PhotoSceneComponent>(), _nextSceneName));

                if (_visorToHide != null)
                {
                    _visorToHide.gameObject.SetActive(false);
                }
            }
        }

        // // Code to have multiple photo for each camera level without changing scene
        // protected LevelPhotosComponent _levelPhotosComponent;
        //
        // protected int _photoIndex = 0;
        //
        // private void Start()
        // {
        //     base.Start();
        //
        //     _levelPhotosComponent = GetComponent<LevelPhotosComponent>();
        //     _photoIndex = 0;
        //
        //     PhotoSceneComponent photoSceneComponent = _levelPhotosComponent._photoScenes[_photoIndex];
        //
        //     // :TODO: Set photo setup on camera button
        //     // :TODO: Add Photo frame and maybe event when camera is validated
        // }
        //
        // protected override void OnValidatedStateChanged(bool isStateVerified)
        // {
        //     // :TODO: check current photo mode state
        //     PhotoSceneComponent currentPhotoSceneComponent = _levelPhotosComponent._photoScenes[_photoIndex];
        //
        //     if (currentPhotoSceneComponent.GetComponent<MachineValidationComponent>().IsValid())
        //     {
        //         // if (_photoIndex + 1 < _levelPhotosComponent._photoScenes.Count)
        //         {
        //             // _eventManager.TriggerEvent(new DisplayPhotoEvent(currentPhotoSceneComponent, this));
        //             // :TODO: show photo that will when clicked on an button load next prefab
        //         }
        //     }
        //     // Switch to photo mode
        //     // _eventManager.TriggerEvent(new ChangeStateEvent(typeof(GameOverState)));
        // }
    }
}