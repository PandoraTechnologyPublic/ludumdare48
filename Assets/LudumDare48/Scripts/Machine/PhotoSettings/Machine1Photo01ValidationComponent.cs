using System.Collections.Generic;
using LudumDare48.GameEvents;
using LudumDare48.Machine;
using UnityEngine;

namespace LudumDare48
{
    public class Machine1Photo01ValidationComponent : PhotoMachineValidatorComponent
    {
        private readonly Dictionary<string, int> _machineObjectives = new Dictionary<string, int>()
        {
            { TriggerType.SUMAFO_MACHINE_SHUTTER_BUTTON.ToString(), 1 },
            { TriggerType.SUMAFO_MACHINE_SCREEN_PHOTO_INDEX_STATE.ToString(), 1 },
            { TriggerType.SUMAFO_MACHINE_SCREEN_STATE.ToString(), 1 },
        };

        private readonly Dictionary<TriggerType, int> _machineDefaultState = new Dictionary<TriggerType, int>()
        {
            { TriggerType.SUMAFO_MACHINE_SHUTTER_BUTTON, 0 },
            { TriggerType.SUMAFO_MACHINE_SCREEN_PHOTO_INDEX_STATE, 0 },
            { TriggerType.SUMAFO_MACHINE_SCREEN_STATE, 0 },
        };

        protected override Dictionary<string, int> GetMachineObjectives()
        {
            return _machineObjectives;
        }

        protected override Dictionary<TriggerType, int> GetMachineDefaultState()
        {
            return _machineDefaultState;
        }
    }
}