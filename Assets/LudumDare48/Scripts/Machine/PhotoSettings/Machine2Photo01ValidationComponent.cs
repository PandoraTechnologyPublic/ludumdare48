using System.Collections.Generic;
using LudumDare48.GameEvents;
using LudumDare48.Machine;
using UnityEngine;

namespace LudumDare48
{
    public class Machine2Photo01ValidationComponent : PhotoMachineValidatorComponent
    {
        private readonly Dictionary<string, int> _machineObjectives = new Dictionary<string, int>()
        {
            { TriggerType.ARGENTIC_MACHINE_SHUTTER_BUTTON.ToString(), 1 },
            { TriggerType.ARGENTIC_MACHINE_SPEED_RING.ToString(), 1 },
            { $"{TriggerType.ARGENTIC_MACHINE_OBJECTIVE_RING}_MIN", 9 },
            { $"{TriggerType.ARGENTIC_MACHINE_OBJECTIVE_RING}_MAX", 10 },
            // { TriggerType.STEAM_MACHINE_OBJECTIVE_RING_1.ToString(), 6 },
            // { TriggerType.STEAM_MACHINE_SCROLL_WHEEL.ToString(), 6 }
        };

        private readonly Dictionary<TriggerType, int> _machineDefaultState = new Dictionary<TriggerType, int>()
        {
            { TriggerType.ARGENTIC_MACHINE_SHUTTER_BUTTON, 0 },
            { TriggerType.ARGENTIC_MACHINE_SPEED_RING, 0 },
            { TriggerType.ARGENTIC_MACHINE_OBJECTIVE_RING, 0 },
            // { TriggerType.ARGENTIC_MACHINE_SPEED_RING, 0 },
            // { TriggerType.STEAM_MACHINE_SCROLL_WHEEL, 0 },
        };

        protected override Dictionary<string, int> GetMachineObjectives()
        {
            return _machineObjectives;
        }

        protected override Dictionary<TriggerType, int> GetMachineDefaultState()
        {
            return _machineDefaultState;
        }
    }
}