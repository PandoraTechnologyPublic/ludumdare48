using System.Collections.Generic;
using LudumDare48.GameEvents;
using LudumDare48.GameState;

namespace LudumDare48.Machine
{
    public class SteamMachineValidationComponent : MachineValidationComponent
    {
        private readonly Dictionary<string, int> _machineObjectives = new Dictionary<string, int>()
        {
            { TriggerType.STEAM_MACHINE_SHUTTER_BUTTON.ToString(), 1 },
            // { TriggerType.STEAM_MACHINE_OBJECTIVE_RING_1.ToString(), 6 },
            // { TriggerType.STEAM_MACHINE_SCROLL_WHEEL.ToString(), 6 }
        };

        private readonly Dictionary<TriggerType, int> _machineDefaultState = new Dictionary<TriggerType, int>()
        {
            { TriggerType.STEAM_MACHINE_SHUTTER_BUTTON, 0 },
            // { TriggerType.STEAM_MACHINE_OBJECTIVE_RING_1, 0 },
            // { TriggerType.STEAM_MACHINE_SCROLL_WHEEL, 0 },
        };

        protected override Dictionary<string, int> GetMachineObjectives()
        {
            return _machineObjectives;
        }

        protected override Dictionary<TriggerType, int> GetMachineDefaultState()
        {
            return _machineDefaultState;
        }
    }
}