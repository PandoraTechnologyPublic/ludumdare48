using LudumDare48.GameEvents;
using UnityEngine;

namespace LudumDare48.Machine
{
    public class Machine1Photo2CustomSettingReactorComponent : ReactorComponent
    {
        private int _currentSettingIndex = 0;

        protected override void ActivatorProcessing(int value)
        {
            PhotoParameters photoParameters = GetComponent<PhotoParameters>();

            if (value == 1)
            {
                _currentSettingIndex = (_currentSettingIndex + 1) % 5;

                switch (_currentSettingIndex)
                {
                    case 0:
                    {
                        photoParameters.Blur = 0.3f;
                        photoParameters.Exp = 0.010f;
                        photoParameters.DOF = 0.35f;
                        photoParameters.FOV = 0.7f;
                        break;
                    }
                    case 1:
                    {
                        photoParameters.Blur = 0.1f;
                        photoParameters.Exp = 0.11f;
                        photoParameters.DOF = 0.4f;
                        photoParameters.FOV = 0.8f;
                        break;
                    }
                    case 2:
                    {
                        photoParameters.Blur = 0.3f;
                        photoParameters.Exp = 0.1f;
                        photoParameters.DOF = 0.35f;
                        photoParameters.FOV = 0.9f;
                        break;
                    }
                    case 3:
                    {
                        photoParameters.Blur = 0;
                        photoParameters.Exp = 0.261f;
                        photoParameters.DOF = 0.422f;
                        photoParameters.FOV = 0.917f;
                        break;
                    }
                    case 4:
                    {
                        photoParameters.DOF = 0.541f;
                        photoParameters.Blur = 0.0f;
                        photoParameters.FOV = 1;
                        photoParameters.Exp = 0.002f;
                        break;
                    }
                }

                _eventManager.TriggerEvent(new TriggerStateChangedEvent(TriggerType.SUMAFO_MACHINE_SCREEN_PHOTO_INDEX_STATE, _currentSettingIndex));
            }
        }
    }
}