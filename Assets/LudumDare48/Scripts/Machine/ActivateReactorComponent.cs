using UnityEngine;

namespace LudumDare48.Machine
{
    public class ActivateReactorComponent : ReactorComponent
    {
        public GameObject _objectToControl;

        protected override void ActivatorProcessing(int value)
        {
            if (value == 1)
            {
                _objectToControl.SetActive(!_objectToControl.activeSelf);
            }
        }
    }
}