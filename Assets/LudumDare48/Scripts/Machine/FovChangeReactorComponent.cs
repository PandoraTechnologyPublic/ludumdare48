using System;
using System.Collections.Generic;
using LudumDare48.GameEvents;
using UnityEngine;
using Zenject;

namespace LudumDare48.Machine
{
    public class FovChangeReactorComponent : MonoBehaviour
    {
        public TriggerType _triggerType;

        private EventManager _eventManager;

        [Inject]
        public void InitDependencies(EventManager eventManager)
        {
            _eventManager = eventManager;
        }

        private void Start()
        {
            _eventManager.Register<TriggerStateChangedEvent>(OnTriggerStateChanged);
        }

        private void OnDestroy()
        {
            _eventManager.Unregister<TriggerStateChangedEvent>(OnTriggerStateChanged);
        }

        private void OnTriggerStateChanged(GameEvent gameEvent)
        {
            TriggerStateChangedEvent triggerStateChangedEvent = (TriggerStateChangedEvent) gameEvent;

            if (triggerStateChangedEvent.TriggerType == _triggerType)
            {
                PhotoParameters photoParameters = GetComponent<PhotoParameters>();
                float photoParametersMaxFov = (photoParameters.maxFOV - photoParameters.minFOV) * triggerStateChangedEvent._rangePosition;
                photoParameters.SetFovValue(Mathf.RoundToInt(photoParametersMaxFov));
            }
        }
    }
}