using UnityEngine;
using UnityEngine.EventSystems;

namespace LudumDare48.Machine
{
    public class SliderTriggerComponent : TriggerComponent
    {
        [SerializeField] private Vector3 _axisDirection = Vector3.back;
        [SerializeField] private int _minValue;
        [SerializeField] private int _maxValue;
        [SerializeField] private float _minPositionValue = 0;
        [SerializeField] private float _maxPositionValue = 10;
        [SerializeField] private float _sensibility = 0.1f;

        private Vector3 _mouseDownPosition;
        public bool _isDragging;

        protected new void Awake()
        {
            base.Awake();
        }

        protected void OnMouseDown()
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                _mouseDownPosition = Input.mousePosition;
            }
        }

        private void OnMouseDrag()
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                if (IsDraggingDisabled())
                {
                    return;
                }

                _isDragging = true;

                float distance = Input.mousePosition.x - _mouseDownPosition.x;

                float sliderValue = Mathf.Clamp(_currentValue + distance * _sensibility, _minValue, _maxValue);

                if (_currentValue != Mathf.RoundToInt(sliderValue))
                {
                    _currentValue = Mathf.RoundToInt(sliderValue);

                    TriggerEvent(_currentValue, _minValue, _maxValue);

                    _mouseDownPosition = Input.mousePosition;
                }

                float positionRange = _maxPositionValue - _minPositionValue;
                int valueRange = _maxValue - _minValue;
                float positionContribution = positionRange / valueRange;

                _transform.localPosition = _axisDirection * (_minPositionValue + sliderValue * positionContribution);
            }
        }

        protected void OnMouseUp()
        {
            _isDragging = false;
        }

        protected virtual bool IsDraggingDisabled()
        {
            return false;
        }
    }
}