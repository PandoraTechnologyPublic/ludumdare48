using DG.Tweening;
using LudumDare48.GameEvents;
using UnityEngine;
using UnityEngine.EventSystems;

namespace LudumDare48.Machine
{
    public class HandleComponent : SliderTriggerComponent
    {
        [Header("Auto snap")]
        [SerializeField] private float _autoSnapDelay;
        [SerializeField] private float _autoSnapDuration;

        private Vector3 _initialLocalPosition;
        private Sequence _autoSnapSequence;
        private bool _isLatchPut;

        protected new void Awake()
        {
            base.Awake();

            _initialLocalPosition = _transform.localPosition;
        }

        protected new void OnMouseDown()
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                base.OnMouseDown();

                if (_autoSnapSequence != null)
                {
                    _autoSnapSequence.Kill();
                    _autoSnapSequence = null;
                }
            }
        }

        protected new void OnMouseUp()
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                base.OnMouseUp();

                AutoSnap();
            }
        }

        private void AutoSnap(bool withDelay = true)
        {
             if (_autoSnapSequence == null && _autoSnapDelay > 0.01f && !_isLatchPut)
             {
                 _autoSnapSequence = DOTween.Sequence()
                     .AppendInterval(withDelay ? _autoSnapDelay : 0)
                     .AppendCallback(() =>
                     {
                         if (!_isLatchPut)
                         {
                            _currentValue = 0;
                            TriggerEvent();
                             _transform.DOLocalMove(_initialLocalPosition, _autoSnapDuration);
                         }
                     })
                     .OnComplete(() => _autoSnapSequence = null);
             }
        }

        protected override void OnTriggerStateChanged(TriggerStateChangedEvent gameEvent)
        {
            if (gameEvent.TriggerType == TriggerType.STEAM_MACHINE_LOCK_LATCH)
            {
                bool wasLatchPut = _isLatchPut;

                _isLatchPut = gameEvent.Value > 30;

                if (wasLatchPut && !_isLatchPut && !_isDragging)
                {
                    AutoSnap(false);
                }
            }
        }

        protected override bool IsDraggingDisabled()
        {
            return _isLatchPut;
        }
    }
}