using System;
using System.Collections;
using DG.Tweening;
using LudumDare48.GameEvents;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;

namespace LudumDare48
{
    public class MainUiLayout : MonoBehaviour
    {
        public Image _fadeScreen;

        [Inject]
        public void InitialiseDependencies(EventManager eventManager)
        {
            eventManager.Register<FadeToNextSceneRequestEvent>(OnFadeRequestEvent);
        }

        private void OnFadeRequestEvent(GameEvent gameEvent)
        {
            StartCoroutine(FadeCoroutine(gameEvent as FadeToNextSceneRequestEvent));
        }

        private IEnumerator FadeCoroutine(FadeToNextSceneRequestEvent fadeToNextSceneRequestEvent)
        {
            _fadeScreen.raycastTarget = true;
            _fadeScreen.DOFade(1, 2.0f);

            yield return new WaitForSeconds(2.0f);

            SceneManager.LoadScene(fadeToNextSceneRequestEvent._nextSceneName);

            _fadeScreen.DOFade(0, 2.0f);
            _fadeScreen.raycastTarget = false;
        }

        public IEnumerator FadeCouroutine(Action action)
        {
            _fadeScreen.raycastTarget = true;
            _fadeScreen.DOFade(1, 1.0f);

            yield return new WaitForSeconds(1.0f);

            action();

            _fadeScreen.DOFade(0, 1.0f);
            _fadeScreen.raycastTarget = false;
        }
    }
}