using System;
using LudumDare48.GameEvents;
using LudumDare48.GameState;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace LudumDare48
{
    public class SmartphoneScreen : MonoBehaviour
    {
        public Button _startButton;

        private EventManager _eventManager;

        [Inject]
        public void InitialiseDependencies(EventManager eventManager)
        {
            _eventManager = eventManager;
        }

        private void Start()
        {
            _startButton.onClick.AddListener(OnStartButton);
        }

        private void OnStartButton()
        {
            _eventManager.TriggerEvent(new ChangeStateEvent(typeof(IngameState)));
        }
    }
}
