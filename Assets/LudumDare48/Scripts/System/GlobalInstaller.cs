using UnityEngine;
using Zenject;

namespace LudumDare48.System
{
    public class GlobalInstaller : MonoInstaller
    {
        [SerializeField] private CursorDecoratorLayout _cursorDecoratorPrefab;
        [SerializeField] private MainUiLayout _mainUiLayoutPrefab;
        [SerializeField] private AudioSource _musicManagerPrefab;

        public override void InstallBindings()
        {
            Container.Bind<EventManager>().AsSingle();
            Container.Bind<CursorDecoratorLayout>().FromComponentInNewPrefab(_cursorDecoratorPrefab).AsSingle().NonLazy();

            Container.Bind<MainUiLayout>().FromComponentInNewPrefab(_mainUiLayoutPrefab).AsSingle().NonLazy();
            Container.Bind<GameManager>().FromNewComponentOnNewGameObject().AsSingle().NonLazy();
            Container.Bind<AudioSource>().FromComponentInNewPrefab(_musicManagerPrefab).AsSingle().NonLazy();
        }
    }
}