using UnityEngine;
using UnityEngine.UI;

namespace LudumDare48.System
{
    public class CursorDecoratorLayout : MonoBehaviour
    {
        [SerializeField] private Image _cursorImage;
        [SerializeField] private Sprite _defaultInteractionSprite;

        private Camera _camera;

        private void Awake()
        {
            _camera = Camera.main;
            _cursorImage.gameObject.SetActive(false);
        }

        public void SetCursorSprite(Sprite cursorSprite)
        {
            if (cursorSprite == null)
            {
                cursorSprite = _defaultInteractionSprite;
            }

            _cursorImage.sprite = cursorSprite;
            _cursorImage.gameObject.SetActive(true);
            Cursor.visible = false;
        }

        public void UnsetCursorSprite()
        {
            _cursorImage.gameObject.SetActive(false);
            Cursor.visible = true;
        }

        public void Update()
        {
            if (_camera == null)
            {
                _camera = Camera.main;
            }

            Vector3 viewportPoint = _camera.ScreenToViewportPoint(Input.mousePosition);
            _cursorImage.rectTransform.anchorMin = viewportPoint;
            _cursorImage.rectTransform.anchorMax = viewportPoint;
        }
    }
}