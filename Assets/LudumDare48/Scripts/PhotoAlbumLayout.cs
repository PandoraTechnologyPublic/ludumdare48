using System;
using UnityEngine;
using UnityEngine.UI;

namespace LudumDare48
{
    public class PhotoAlbumLayout : MonoBehaviour
    {
        public Button _albumDisplayButton;
        public RectTransform _albumPanel;

        private void Awake()
        {
            _albumDisplayButton.onClick.AddListener(OnAlbumDisplayButton);
            _albumPanel.gameObject.SetActive(false);
        }

        private void OnAlbumDisplayButton()
        {
            _albumPanel.gameObject.SetActive(!_albumPanel.gameObject.activeSelf);
        }
    }
}