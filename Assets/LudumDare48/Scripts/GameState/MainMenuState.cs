using System;
using LudumDare48.GameEvents;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace LudumDare48.GameState
{
    public class MainMenuState : State<GameManager>
    {
        private EventManager _eventManager;
        private Action<GameEvent> _onChangeStateMessage;

        [Inject]
        public void InitialiseDependencies(EventManager eventManager)
        {
            _eventManager = eventManager;
        }

        public override void Enter(GameManager gameManager)
        {
            _eventManager.TriggerEvent(new EnterGameStateEvent(GetType()));
            _onChangeStateMessage = (gameEvent) => OnChangeStateMessage(gameManager, gameEvent);
            _eventManager.Register<ChangeStateEvent>(_onChangeStateMessage);
        }

        private void OnChangeStateMessage(GameManager gameManager, GameEvent gameEvent)
        {
            ChangeStateEvent changeStateEvent = gameEvent as ChangeStateEvent;

            if (changeStateEvent._newStateType == typeof(IngameState))
            {
                gameManager.ChangeState<IngameState>();
            }
        }

        public override void Update(GameManager gameManager, float timeStep)
        {
            if (SceneManager.GetActiveScene().name != "MainScene")
            {
                _eventManager.TriggerEvent(new ChangeStateEvent(typeof(IngameState)));
            }
        }

        public override void Exit(GameManager gameManager)
        {
            _eventManager.TriggerEvent(new ExitGameStateEvent(GetType()));
            _eventManager.Unregister<ChangeStateEvent>(_onChangeStateMessage);
        }
    }
}