using System;
using LudumDare48.GameEvents;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;
using NotImplementedException = System.NotImplementedException;

namespace LudumDare48.GameState
{
    public class IngameState : State<GameManager>
    {
        private EventManager _eventManager;
        private Action<GameEvent> _onChangeStateMessage;

        [Inject]
        public void InitialiseDependencies(EventManager eventManager)
        {
            _eventManager = eventManager;
        }

        public override void Enter(GameManager gameManager)
        {
            _eventManager.TriggerEvent(new EnterGameStateEvent(GetType()));
            _onChangeStateMessage = (gameEvent) => OnChangeStateMessage(gameManager, gameEvent);
            _eventManager.Register<ChangeStateEvent>(_onChangeStateMessage);

            if (SceneManager.GetActiveScene().name == "MainScene")
            {
                _eventManager.TriggerEvent(new FadeToNextSceneRequestEvent("Machine1Photo01"));
            }
        }

        private void OnChangeStateMessage(GameManager gameManager, GameEvent gameEvent)
        {
            ChangeStateEvent changeStateEvent = gameEvent as ChangeStateEvent;

            if (changeStateEvent._newStateType == typeof(GameOverState))
            {
                gameManager.ChangeState<GameOverState>();
            }
        }

        public override void Update(GameManager owner, float timeStep)
        {
            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyUp(KeyCode.W))
            {
                _eventManager.TriggerEvent(new ChangeStateEvent(typeof(GameOverState)));
            }
        }

        public override void Exit(GameManager owner)
        {
            _eventManager.TriggerEvent(new ExitGameStateEvent(GetType()));
            _eventManager.Unregister<ChangeStateEvent>(_onChangeStateMessage);
        }
    }
}