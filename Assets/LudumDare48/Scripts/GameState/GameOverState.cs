using System;
using LudumDare48.GameEvents;
using UnityEngine.SceneManagement;
using Zenject;

namespace LudumDare48.GameState
{
    public class GameOverState : State<GameManager>
    {
        private EventManager _eventManager;
        private Action<GameEvent> _onChangeStateMessage;

        [Inject]
        public void InitialiseDependencies(EventManager eventManager)
        {
            _eventManager = eventManager;
        }

        public override void Enter(GameManager gameManager)
        {
            _eventManager.TriggerEvent(new EnterGameStateEvent(GetType()));
            _onChangeStateMessage = (gameEvent) => OnChangeStateMessage(gameManager, gameEvent);
            _eventManager.Register<ChangeStateEvent>(_onChangeStateMessage);
        }

        private void OnChangeStateMessage(GameManager gameManager, GameEvent gameEvent)
        {
            ChangeStateEvent changeStateEvent = gameEvent as ChangeStateEvent;

            if (changeStateEvent._newStateType == typeof(MainMenuState))
            {
                _eventManager.TriggerEvent(new FadeToNextSceneRequestEvent("MainScene"));

                void OnSceneManagerOnActiveSceneChanged(Scene arg0, Scene arg1)
                {
                    gameManager.ChangeState<MainMenuState>();
                    SceneManager.activeSceneChanged -= OnSceneManagerOnActiveSceneChanged;
                }

                SceneManager.activeSceneChanged += OnSceneManagerOnActiveSceneChanged;
            }
        }

        public override void Update(GameManager gameManager, float timeStep)
        {

        }

        public override void Exit(GameManager gameManager)
        {
            _eventManager.TriggerEvent(new ExitGameStateEvent(GetType()));
        }
    }
}