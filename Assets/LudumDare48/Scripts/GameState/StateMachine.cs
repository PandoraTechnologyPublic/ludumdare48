using Zenject;

namespace LudumDare48.GameState
{
    public class StateMachine<TYPE_OWNER>
    {
        private State<TYPE_OWNER> _currentState;
        private DiContainer _diContainer;

        [Inject]
        public void InitialiseDependencies(DiContainer diContainer)
        {
            _diContainer = diContainer;
        }

        public void Update(TYPE_OWNER owner, float timeStep)
        {
            _currentState.Update(owner, timeStep);
        }

        public bool IsCurrentState<TYPE_STATE>()
        {
            return typeof(TYPE_STATE) == _currentState.GetType();
        }

        public void ChangeState<TYPE_STATE>(TYPE_OWNER owner) where TYPE_STATE : State<TYPE_OWNER>, new()
        {
            if ((_currentState == null) || !IsCurrentState<TYPE_STATE>())
            {
                if (_currentState != null)
                {
                    _currentState.Exit(owner);
                }

                _currentState = new TYPE_STATE();
                _diContainer.Inject(_currentState);
                _currentState.Enter(owner);
            }
        }
    }
}