namespace LudumDare48.GameState
{
    public abstract class State<TYPE_OWNER>
    {
        public abstract void Enter(TYPE_OWNER owner);

        public abstract void Update(TYPE_OWNER owner, float timeStep);

        public abstract void Exit(TYPE_OWNER owner);
    }
}