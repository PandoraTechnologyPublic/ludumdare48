using System.Collections.Generic;
using DG.Tweening;
using LudumDare48.GameEvents;
using LudumDare48.GameState;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace LudumDare48
{
    public class IngameUiLayout : MonoBehaviour
    {
        public RectTransform _frameDock;

        private EventManager _eventManager;
        private PhotoLayout _photoLayout;
        private DiContainer _diContainer;
        private MainUiLayout _mainUiLayout;

        [Inject]
        public void InitialisedDependencies(EventManager eventManager, DiContainer diContainer, MainUiLayout mainUiLayout)
        {
            gameObject.SetActive(false);
            HidePhoto();

            eventManager.Register<EnterGameStateEvent>(OnEnterGameState);
            eventManager.Register<ExitGameStateEvent>(OnExitGameState);
            eventManager.Register<DisplayPhotoEvent>(OnDisplayPhotoEvent);

            _eventManager = eventManager;
            _diContainer = diContainer;
            _mainUiLayout = mainUiLayout;
        }

        private void OnDisplayPhotoEvent(GameEvent gameEvent)
        {
            DisplayPhotoEvent displayPhotoEvent = gameEvent as DisplayPhotoEvent;

            StartCoroutine(_mainUiLayout.FadeCouroutine(() => ShowPhoto(displayPhotoEvent)));
            // .StartFadeCoroutine(FadeCouroutine())

            ;
        }

        private void ShowPhoto(DisplayPhotoEvent displayPhotoEvent)
        {
            _photoLayout = GameObject.Instantiate(displayPhotoEvent._photoSceneComponent._finalPhoto, _frameDock);
            _diContainer.Inject(_photoLayout);
            _photoLayout.gameObject.SetActive(true);
            _photoLayout.SetDisplayEvent(displayPhotoEvent, this);
        }

        public void HidePhoto()
        {
            if (_photoLayout != null)
            {
                DOTween.Sequence().AppendInterval(2.0f).AppendCallback(() =>
                {
                    _photoLayout.gameObject.SetActive(false);
                    GameObject.Destroy(_photoLayout.gameObject);
                });
                // _frameDock.gameObject.SetActive(false);

            }
        }

        private void OnEnterGameState(GameEvent gameEvent)
        {
            EnterGameStateEvent enterGameStateEvent = gameEvent as EnterGameStateEvent;

            if (enterGameStateEvent._stateType == typeof(IngameState))
            {
                gameObject.SetActive(true);
            }
        }

        private void OnExitGameState(GameEvent gameEvent)
        {
            ExitGameStateEvent exitGameStateEvent = gameEvent as ExitGameStateEvent;

            if (exitGameStateEvent._stateType == typeof(IngameState))
            {
                gameObject.SetActive(false);
            }
        }
    }
}